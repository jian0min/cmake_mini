all:
	mkdir build
	cd build ; cmake .. -G "Unix Makefiles"

run:
	cd build; cmake --build .
	./build/src/main

clean:
	rm -rf build
