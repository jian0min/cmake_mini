
project(hello_main)

#set (PROJECT_SOURCES main.cc)

add_subdirectory(math_)

add_executable(main main.cc)
target_link_libraries(main PRIVATE math)
